# LaTeX user guide compilation

To compile user guide:

1. `pdflatex obs-en`
2. `bibtex obs-en`
3. `pdflatex obs-en`
4. `pdflatex obs-en`

or

use the makefile:

`make`

