\documentclass[]{ceurart}
\sloppy
\usepackage{listings}
\lstset{breaklines=true}

\begin{document}

\copyrightyear{2023}
\copyrightclause{Copyright for this paper by its authors.
  Use permitted under Creative Commons License Attribution 4.0
  International (CC BY 4.0).}

\conference{In:
B. Combemale, G. Mussbacher, S. Betz, A. Friday, I. Hadar, J. Sallou,
I. Groher, H. Muccini, O. Le Meur, C. Herglotz, E. Eriksson,
B. Penzenstadler, AK. Peters, C. C. Venters.  Joint Proceedings of
ICT4S 2023 Doctoral Symposium, Demonstrations \& Posters Track and
Workshops. Co-located with ICT4S 2023. Rennes, France, June 05-09,
2023.}

\title{Digital obsolescence}

\author[1]{Edlira Nano}[
orcid=0000-0003-2166-5231,
email=eda@mutu.net,
url=https://eda.mutu.net,
]
\address[1]{Univ Lyon, UCBL, CNRS, INSA Lyon, Centrale Lyon, Univ Lyon
  2, LIRIS, Limites numériques project, UMR5205, F-69622,
  Villeurbanne, France}

\begin{abstract}
Within a framework of study of the ecological impact of digital
technology, this PhD project aims at understanding and analysing
digital obsolescence. It aims to analyse software, hardware and
middle-ware obsolescence, its technical aspects and its intertwining
with non-technical ones such as economical, psychological, human and
environmental obsolescence.
The study will first focus on smartphones, whose rapid pace of
development and replacement allows a better characterisation of digital
obsolescence. The second object of our study is digital obsolescence
within Debian, a widespread established operational system based on
the Linux kernel, maintained by a large community that organises
itself in a non profit organisation, following the principles of open
source code, collaboration, free distribution and sharing for everyone. 
Finally, the study will dive into some alternative community
networks that profoundly question existing technologies and are shaped
and built with the central idea of care for ourselves, for each-other
and for the environment.

\end{abstract}

\begin{keywords}
  digital obsolescence \sep
  environmental digital impact \sep
  smartphones \sep
  Debian \sep
  community networks \sep
  alternative technology \sep
  care in technology
\end{keywords}

\maketitle

\begin{center}
\includegraphics[width=\linewidth, height=7cm]{images/P2230133.JPG}
\end{center}


\section{Research context and motivation}

This PhD thesis began in January 2023 at the Unviversité Lyon 1 in
France, department of Computer Science, under the supervision of
Aurélien Tabard\footnote{https://tabard.fr/}, within the \emph{Limites
Numériques}\footnote{https://limitesnumeriques.fr/} team.

\emph{Limites Numériques} is a research project on the
environmental footprint of digital technologies that explores design
choices, uses and functions of digital technology within planetary
limits.

My background is in theoretical computer science and
program development. Before joining the team, I was an independent
free software developer and program coordinator. I am a digital rights
and freedoms advocate at the nonprofit organisations La Quadrature du
Net\footnote{\url{https://laquadrature.net/en/}} and
April\footnote{https://april.org/en}. As such, I have participated in
campaigns and workshops on digital empowerment and protection of
fundamental rights in ICT, dealing with issues of
repair, reuse and reinstall, as well as digital security,
encryption and anti-surveillance ones.

The study of digital obsolescence during this PhD thesis is a
continuation of my previous work, but it is above all, an
opportunity to take a step back and focus on the ecological and
environmental aspects of technology.

\section{Background}

In her PhD thesis in 2019, called \emph{Obsolescence : the philosophy
of technology and economic history regarding the shortening of
objects’ lifespans}\cite{guien_obsolescences_2019}\footnote{Guien has
also published a book from her thesis called \emph{Consumerism through
its objects\cite{jeanne_guien_consumerisme_2021}}}, Jeanne Guien goes
through the history of some common everyday consumer goods such as
smartphones, plastic glasses, paper tissues, deodorisers. She shows
how, far from having been practised in secret, obsolescence of goods
has been publicly promoted, and continues to be, as a source of
progress, prosperity, equality or emancipation. For Guien, the
systematic renewal of objects has been erected as a sign, a source,
and even the essence  of their value\cite{guien_quest-ce_2020}. To
Guien, this helps clarify the role that obsolescence and waste play in
Western economies, and allows to question the limits of current
sustainability policies.

As for digital technologies, while their environmental impact is
steadily growing\cite{freitag_real_2021}, few studies analyse all the
mechanisms at play on their obsolescence through their
lifespan. In the scientific research literature obsolescence is often
defined and analysed from the point of view of managing obsolescence of 
information systems in an industrial, military or professional context
(see \cite{sandborn_software_2008} or \cite{bartels_software_2012}),
focusing on solutions to maintain and not on the reasons behind. In
France, debate and public policies on digital obsolescence
historically focused on hidden planned obsolescence. A French 2021
parliamentary report on software obsolescence
\cite{Castellazzi_obsolescence_2021}, defines digital
obsolescence as a special case of technical obsolescence, as the loss 
of use or value resulting from a purely and solely technical evolution.

Based on these observations and continuing in the footsteps of the
work from Jeanne Guien, this thesis will first try to analyse how
obsolescence is situated through the history of the development and
spread of modern digital technology. By choosing to focus on specific
objects, and studying their spread and usage historically, the aim is
to analyse software, hardware and middle ware obsolescence, its
technical aspects but also socio-economical, political, psychological
and human factors that are at stake and intertwined in digital
obsolescence.

\section{Research goals and questions}
The study will first focus on two digital objects:
\begin{itemize}
\item smartphones on one hand, that hold a rapid growth and
  significant obsolescence issues, fuelled by economic discourses and
  marketing policies based on promotion of technology
  ``innovation'',  of design-related features or trends, accompanied
  by a strong and rapid change in usage patterns;
\item the Debian operating system on the other hand, as a
  lasting well established operating system since 1993, that seems to
  address issues of sustainability and longevity in a different way,
  away from economic and mercantile traditional digital
  markets, more focused on durability, stability, maintenance,
  collaboration, sharing of code and knowledge.
\end{itemize}
Finally, the study will dive into alternative ways of
building technologies through examples of community networks that
build digital tools based on their exact needs, by questioning
problems of present mainstream technologies, taking care not to
reproduce them, and instead putting main focus on the care of
one-selves, of each-other and of the environment.

\subsection{Digital obsolescence through smartphones}

As the environmental impact of ICT grows\cite{freitag_real_2021},
smartphones continue to spread wider, to be used more, and to be
renewed very frequently, while being highly tied to digital physical
and software infrastructures (networks, datacenters, servers, software
platforms). In France in 2019, the average age of smartphones in use
was estimated at 32 months in 2021\cite{arcep_renouvellement_2021},
while a recent study on European consumers estimates smartphone
renewal after 43 months of use\cite{magnier_replaced_2022}.

\subsubsection{Technical obsolescence and technology promotion
  narratives}

The market of smartphones offers and promotes new model
devices at unrivalled frequent rates. At the same time, there is a
very rapid release rate of major smartphone operating systems 
(Apple iOS every one year, Google Android OS every 6
months), while existing devices are rarely updated
to the newly released systems (Android's new OS rarely exceeds 35\% of
device coverage at its most\footnote{see e.g. statistics 
\href{https://www.statista.com/statistics/271774/share-of-android-platforms-on-mobile-devices-with-android-os/}{here},
last accessed April 23, 2023}). As a consequence, functioning
smartphones quickly and widely carry unmaintained systems with neither
important security updates, nor new features, while applications and
services are constantly being developed or reshaped to fit the newest
operating systems and newest device models with higher hardware
performances. This repeated cycle of renewal, is
accompanied by an advertising and mercantile staging
that promotes innovation, novelty, change, and the need and urge to
adopt them. These notions, that have shaped our digital modern life,
hold obsolescence by design: by definition, innovation is temporary, and
novelty is an ephemeral quality.

This thesis will try to qualify and quantify the intertwining of
hardware and software obsolescence in smartphones, during their
historical development, production, promotion, spread and renewal
cycle. We will do so by analysing technology promotion narratives, but
also technical data gathered on some selected smartphone models. We
will interview developers working on hardware and software
maintenance issues at manufacturers, application developers, but also
online communities building alternative and long-term maintained OSs
for older devices, such as
LineageOS\footnote{\url{https://lineageos.org/}} and LineageOS for 
microG\footnote{\url{https://lineage.microg.org/}}, two 
free and open source community-developed Android-based OSs.

So far, as represented in Figure \ref{fig:layers}, we have identified
the following smartphone layers in which obsolescence mostly appears:
the \emph{hardware} layer, the \emph{middle-ware} layer, the
\emph{operational system} (OS) layer, the \emph{manufacturers and
operators} layer and the \emph{application} layer. We also identified
two layers that seem to interact in an intersectional way with the
above ones: the \emph{SoC integration} layer and the \emph{background
services} one. During this thesis we will analyse how obsolescence is
created and is enhanced in each of these layers.

\begin{figure}
  \centering
  \includegraphics[width=0.35\linewidth]{images/smartphone-layers}
  \caption{Smartphone layers where obsolescence is observed}
  \label{fig:layers}
\end{figure}
 
\subsubsection{SoC integration obsolescence}

In smartphones, hardware components are often materially designed and
manufactured as integrated circuits that combine different units in
one silicon plate. System-on-chip (SoC) is one of the most widely
spread integrated circuit in smartphones, tablets or other IoT. An SoC
generally contains the Central Processing Unit (CPU), together with
other central processing units (memory, modems, sensors ...),
all manufactured together in a single chip soldered onto the
circuit board. SoCs also include a software component (called
firmware) executed on the processing elements to control the
peripherals and interfaces.

During the personal computers (PC) era, the use of microprocessors
with separate controller chips has been the norm, giving more
flexibility in assembling and repairing devices. The drive toward
smartphones, tablets and IoT has pushed integration further: putting
more elements on a single piece of silicon as happens with SoCs is
said to reduce cost and physical size, which helps create smaller,
flatter smartphones that use less battery life. The size and thinness
aspects have been key selling features in smartphones, playing a
significant role in economical cross-brand competition and smartphone
designing trends. Lately, SoCs and integrated circuits are being more
and more used in PCs, with the same promise of flatter, thinner and
easier mobility. By analysing the history of their economical
promotion and spread, we can see how SoCs have accompanied the
rapid renewal and disposable aspect of smartphone devices.

We think that SoCs are likely to be at the chunter of software and
hardware obsolescence in smartphones by reducing hardware flexibility,
making repair and parts replacement far more complicated on the
hardware side, and on the software side also by being the main reason
for the early end of OS versions maintenance and update. 

We will focus the analysis on the three major smartphone SoC
manufacturers: Apple Silicon SoCs on one hand, Qualcomm and MediaTek
in the Android market on the other hand\footnote{see the
market study from 
\href{https://www.counterpointresearch.com/android-smartphone-soc-market-2021}{Counterpoint},
last accessed on April 29, 2023}. The impact of SoC integration in OS
maintenance will be analysed through the example of the maintenance
process of Android OS in
Fairphone\footnote{https://www.fairphone.com/} smartphones. Fairphone
development team has been reporting and giving feedback on their OS
maintenance issues, much related to SoC integration
problems. Fairphone has nevertheless been offering long-term OS
maintenance on their old devices, based on work with LineageOs and
other alternative free and open-source (FOSS) initiatives. We will try
to understand how these FOSS initiatives and Fairphone circumvent
no-longer maintained firmware, and develop their own code to offer
further OS maintenance for old to very old devices.

\subsubsection{Background services that cause obsolescence}

At the OS and applications level, smartphones are often equipped
with system-like background services, which are often software based
cloud-connectivity technologies such as Google Play Services (GPS) or
Google's Firebase Cloud Messaging (FCM). 

Introduced in 2012, Google Play Services are now widely generalised in
all Android devices and applications. Android applications are
being developed specifically to interact with them. This dependency
has come to the point where many simple-purposed apps like the Clock
app, or the Calculator app, that do not fundamentally need them, that
used to work historically without them in previous OS versions, now
function only when Google Play Services  are enabled and trigger panic
warnings when not (see figure \ref{fig:panic}).

\begin{figure}
  \centering
  \includegraphics[width=0.28\textwidth]{images/Screenshot_GPS_en}
  \caption{Screenshot of a Fairphone 3 with Google Android 11:
    after deactivation of Google Play Services, basic applications
    such as Phone, Messages, Clock or Contacts, warn repeatedly and
    worryingly that they won't function without GPS. Most of them do
    seem to work quite normally though.}
  \label{fig:panic}
\end{figure}

The Google Firebase Cloud Messaging is a cross-platform cloud service
for messages and push notifications for Android, iOS, and web
applications. In Android, FCM is strongly related to Google Play
Sevices. Over the years Google made changes to Android OS, making
it harder not to use FCM for push notifications.

Both FCM and GPS services are proprietary software, privately owned
and developed by Google in a closed manner. They are based on Google's
cloud hardware and software infrastructure (datacenters,
servers...). If the Android system is open-source, its strong 
dependancy to proprietary background services and infrastructure, 
makes the bare open-source version of Android quite difficult to be
used alone, making in practice the Android system dependent on
maintenance decisions from Google.

Not only are these background services forcing strong, sometimes
unnecessary network connectivity and cloud infrastructure use, but
they also reduce development flexibility and long lasting software
possibilities. Indeed, an application developer will more
likely implement Google private background services, and not bother
duplicate code for smaller use markets. Nevertheless, more recently,
was developed UnifiedPush\footnote{https://unifiedpush.org/}, a set of
free and  open-source specifications and tools for push services that
follows a standard protocol. If implemented, UnifiedPush allows an
application developer to use the push services that the phone offers,
be it Google Firebase or others.

In our study we will analyse the history of the creation and spread of
major non-standardised background services, and of the way they
control the software ecosystem in smartphones, forcing dependencies
and creating obsolescence. We will also be exploring some 
community-driven and alternative circumventing solutions such as
e.g. LineageOs for MicroG - which adds a free and open source
implementation of Google Play Services to LineageOs -, or UnifiedPush
that standardises use of push notifications.

\subsubsection{Connectivity obsolescence}

Smartphones are highly connected devices whose usage is permanently
connected.

First, there is a permanent connectivity to the Internet in
smartphones, that is assumed by design in the OSs - as we can see with
the Google Play Services example -, and in most of applications,
which offer very little offline use possibilities. This
permanently-connected use of smartphones has been democratised little
by little, from the first phones that were not or very
little connected, until appearing today as a preponderant, mandatory
or inevitable use. Cutting the connectivity of a smartphone today
means breaking a large number of services, both at the system and at
the application level, making the device quite difficult to be used.

Moreover, there is almost no way to turn off all
connectivity on a smartphone. Around 2010 smartphone devices began to
have non removable batteries and sealed covers. Before this, cutting the
connectivity was still possible by ultimately removing the battery and
thus switching off the phone in a hardware-based way. With the arrival
of non-removable batteries in almost all smartphones, not only has
repairability been affected, but also connectivity. A software-based
powering-off does not mean that the phone is actually off: in recent
devices, services like Google Android's or Apple's  remote tracking,
designed to be used to locate the phone in case of loss or theft,
always remain on, even after the so called ``powering-off''. 

As data continuously flows in and out of modern smartphones, invisible
and invisibilised digital infrastructure is used to ensure constant
connectivity and data flow. At the same time, phone internet providers
offer unlimited or large data consumption subscriptions, also
transforming our use habits and pushing towards perpetual connectivity.
The arrival of the 5G connectivity technology in recent years, promising
bigger and faster data transfer, is presented as a necessary
technical adaptation to modern uses, but in reality the path towards
unlimited data flows and perpetual smartphone connectivity was already
traced for users before the advent and spread of 5G technology.

This thesis will try to analyse the obsolescence behind the
historically induced connectivity in smartphones.

\subsubsection{The experience of obsolescence: human and
  social obsolescence}

The experience of obsolescence has already been a focus in the
\emph{Limites numériques} team where this thesis is taking place, in
particular through the work of our team member Léa Mosesso on
obsolescence paths\cite{mosesso_obsolescence_2023}. In the footsteps
of Lea's work, during this thesis we will organise workshops to
question and analyse smartphone experiences of obsolescence.

During this study we  will consider the links between digital
obsolescence and the new digital human oppression on workers. First,
the difficult work and human conditions in the Global South factories
that manufacture smartphones and components, for example at Foxconn
factories in Asia, where most of Apple smartphones are produced and
where there is a big worker suicidal
rate\cite{xu_lizhi_machine_2015}. But also, the work of subcontracted
developers, such as the so-called "click
workers"\cite{antonio_a_casilli_en_2019}.

Moreover, some digital uses such as digital surveillance, digital
violence, discrimination or oppression, are all new forms of violence
enhanced by an increasing digital usage in our societies, that
seriously deteriorate living and working conditions and violate
fundamental human rights. This thesis will consider these human
factors as an important part of the ecological footprint of digital
technologies and will analyse the related digital obsolescence at stake.

\subsubsection{Environmental obsolescence}

Environmental impact of digital obsolescence can be observed in all
three life phases of smartphones:
\begin{itemize}
\item the extraction and manufacturing phase, where we have energy,
  water and material consumption, land and water pollution,
  diswatering, biodiversity changes, human population impacts and
  geopolitical conflicts, all of these being mostly located in Global
  South countries or indigenous land; 
\item during marketing and usage phase, mostly located on rich
  territories but more and more worldwide spread, we have device
  obsolescence but also infrastructure-caused obsolescence:
  territorial impact of datacenter spread, production of carbonated
  energy needed for powering up, advertisement campaigns impact
  (energy consumption, induced device renewal, mental and visual
  pollution for humans);
\item and finally, during the end-of-life phase, we encounter
  insufficient digital waste management (on Lithium-Ion
  batteries, on precious metals and rare-earth elements, on
  plastic) and pollution caused by waste being buried,
  burned, spread out on land or transferred in Global South countries.
\end{itemize}

This thesis will present state of the art research and analysis on
these issues, without analysing them in detail, but
considering them throughout the course of our study.

\subsection{The case of a lasting operational system, Debian}

The Debian Operational System (OS) is one of the oldest and most
widespread OS. Based on the open source Linux kernel, composed of
free and open source software, Debian is known to be a very stable and
reliable OS, serving as a basis for many other Linux OS
distributions\footnote{118 as of today according to 
\href{https://distrowatch.com/search.php?ostype=All&category=All&origin=All&basedon=Debian&notbasedon=None&desktop=All&architecture=All&status=Active}{Distrowatch},
last accessed on March 16th, 2023}, most notably Ubuntu, Linux, Mint,
Tails, Yunohost, Raspberry Pi OS, etc. It began development in 1993
and is nowadays quite widespread among web-servers
worldwide\footnote{Debian and Ubuntu were used on 48,5\% of all
web-servers in 2023 according to 
\href{https://web.archive.org/web/20150806093859/http://www.w3cook.com/os/summary/}{w3cook}},
embedded systems, microcontrolers, etc. 

Debian supports a wide variety of hardware architectures, and can be
installed in quite old computers offering a
good experience of usability, where other OS are not able
to install, or perform poorly and slowly. It is also one of the OSs
that offers the longest support time through its Debian Long Term
Support (LTS) and Very Long Term Support (VLTS) programs
shown on figure \ref{fig:debian}.

\begin{figure}
  \centering
  \includegraphics[width=0.85\linewidth]{images/debian}
  \caption{Support periods in Debian (source \href{https://web.archive.org/web/20230000000000*/https://www.freexian.com/lts/debian/}{Freexian}, last
    accessed on April 24, 2023)}
  \label{fig:debian}
\end{figure}

Since its founding, Debian has been developed openly and distributed
freely. The project is coordinated by a team of more than a thousand
volunteers worldwide, that meet online and regularly in real person
gatherings. The team is guided by a Debian Project Leader elected
every two years, and three foundational documents: the Debian Social
Contract, the Debian Constitution, and the Debian Free Software
Guidelines.

The Debian project and its community have been analysed in a quite large
number of research papers from the engineering point of
view\cite{gonzalez-barahona_macro-level_2009}, the economical point of
view\cite{mateos-garcia_institutions_2008} or anthropological and
social
ones\cite{schoonmaker_hacking_2012}\cite{coleman_three_2005}. By
exploring state of the art on Debian, followed by field
research in the community, this thesis will analyse how Debian deals
with obsolescence issues such as maintenance and long term support,
stability, durability or retro-compatibility.

\subsection{Alternative technologies: building community networks
  based on care}

Lastly, during this PhD thesis I would like to study some community
networks across the world, usually in areas lacking network
infrastructure, or where the existing one does not  match community
criteria. Let us take some examples:

\subsubsection{The \emph{Nós por Nós}, \emph{Nodes that bond} project}

The \emph{Nodes that bond} project is a rural women community project in
  Brazil. Created in 2019, the project \emph{"sprouts from the need to
  occupy the virtual territory with feminist narratives. During
  circular meetings allied to basic technology tutorials we began to
  weave collective knowledge through the creation of common
  ground. Knowledge is never generic, it only ever exists applied to
  territory and context. This  understanding is very important to help
  us generate and manage our autonomous network in the best
  way. Beyond the virtual connection,  we seek to keep women connected
  to each other, learn more about the territory we inhabit and
  manifest technology as a
  practice."}\footnote{\url{https://portalsemporteiras.github.io/en/nos-por-nos}}

  During the first year, the community organised women circles of
  relating, learning and tutoring together (see examples in figures
  \ref{fig:tutoring}), summarized in the
  \emph{Nodes for bond workbook}\cite{luisa_bagope_nodes_2021}. In the
  second  year in 2020, the community created a collective local monthly
  podcast and an audio novella.

  \begin{figure}
  \centering
  \includegraphics[width=0.70\linewidth]{images/tutoring-nos-por-nos}
  \caption{Extract from the Nós por Nós workbook: tutoring on sensors of
    smartphones}
  \label{fig:tutoring}
  \end{figure}
  
  \begin{figure}
  \centering
  \includegraphics[width=0.70\textwidth]{images/crack-nos-por-nos}
  \caption{Extract from the Nós por Nós workbook: class on computer
    components}
  \label{fig:crack}
  \end{figure}

  \subsubsection{\emph{Red del viento} in Columbia}
  \emph{Red del viento} is a community network providing internet and
  intranet access, created in Columbia in 2020 and formed by
  Indigenous communities of the Nasa people, FARC ex-combatants and
  peasant local communities, so that they could have their own means of
  communication\footnote{\url{https://www.apc.org/en/news/community-networks-latin-america-weaving-dreams-together}}.
  This project allowed the creation of a WiFi network in the region,
  but above all brought different people together in an
  auto-organised community and self-educational process.

  By studying community networks during a period of 9 to 12 months
  in the middle of the thesis, I hope to find new perspectives on
  digital obsolescence, and on how to shape alternative
  digital tools that focus on care for ourselves, for
  each-other and for the environment.

\section{Research approach and methods}

The first approach of this PhD thesis will be a literature study of the
history of digital technology and of state of the art literature
on obsolescence and environmental impact of ICT.

A parallel approach, will be that of inquiries and field surveys. In
my team there is a strong experience in inquiries, which I think
will be of great benefit to me. I believe that most of my inquiries
will be semi-directive interviews, and field survey will
consist of real-life or online immersions whenever it is possible into
the communities events and meetings. I will follow the meetings of the
Debian community, and conduct personal or collective interviews with
its members following my research goals, and revisiting them during
the process.

I would also like to organise smartphone workshops, where we gather
and talk about our experiences with our smartphones, and help each
other understand obsolescence issues and maybe try technical or non
technical solutions.

Finally, I hope to be able to spend several months between my second and
third year of PhD, in at least one community network such as those
mentioned above, by the mean of a fellowship within the Association
for Progressive Communications\footnote{https://apc.org}, an
international nonprofit organisation that helps and funds community
networks, or other similar host organisations.

\section{Results, dissertation status and next steps}

In the first months of this PhD, I have began the
literature study of the subject, and structured the analysis that I
am going to conduct on smartphones. I have prepared some first
interviews (Fairphone), and have contacted the Debian community.
My next steps would be to perform the interviews and to finish a first
round of analysis on smartphone data. Next year, I would like to dive
more precisely into the Debian community and to centre my study around
key issues related to obsolescence and Debian. At the beginning of the
2nd year, I will also actively search for financing my deep dive
period of 9 to 12 months into community networks, and prepare this
study that would take place between my 2nd and 3rd year. In my 3rd
year I am planning on analysing all my data, and organising my results
and thoughts while writing the thesis.

By discussing this plan of work within the ICT for sustainability
community, I hope to benefit from the critiques and suggestions around
my research questions. I would also very much appreciate feedback and
advise on interview conducting and survey methods, in particular on
survey or interview biases and ways to avoid them. I would also
appreciate external feedback on the choices of my three study objects.

Last but not least, I very much welcome any feedback, warning and
advise on conducting a PhD thesis.

\bibliography{obs-en}

\end{document}
